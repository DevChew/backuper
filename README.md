# Backuper

```sh
     ____  ___   ________ ____  ______  __________
    / __ )/   | / ____/ //_/ / / / __ \/ ____/ __ \
   / __  / /| |/ /   / ,< / / / / /_/ / __/ / /_/ /
  / /_/ / ___ / /___/ /| / /_/ / ____/ /___/ _, _/
 /_____/_/  |_\____/_/ |_\____/_/   /_____/_/ |_|
```

## install

* `sh install.sh`
* add cron `sudo ln -s /home/example/make-backup.sh /etc/cron.daily/make-backup.sh` - need to check that

## singleBackup

backup single domain based on settings in config file passed in argument

in `settings/domains` you need to add config file, for example:
`domain.com.config`

filename will be used as domain name, and folder name in backup catalog.

this file need to have this options:

```sh
sourceDomainCatalog="home/user/htdocs/domain.com" # absolute to folder to backup ~/ will be added on front of path, witchout leading slash!
sqlUsername="user" # sql user
sqlPassword="password" # sql pass
sqlDatabaseName="db_name" # sql database name
```

run `singleBackup.sh domain.com` and script pack catalog and sql to `domains/[domain.com]`

## backupAll

backup all domains

basicly it run singleBaskup.sh for each `.config` file in `settings/domains` folder

## cronBackup

runs backupAll

## restoreWizard

wizard for restoring

## configWizard

run config wizard to create backup config file

add parameter wp to get database info from wordpress wp-config.php file

## todo

* encrypt config files, backup all shoul need master key to run [based on this](https://superuser.com/questions/20549/how-can-i-encrypt-a-string-in-the-shell)
