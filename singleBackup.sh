#!/bin/bash

# get config filename variable, from argumnet
# the argument shold be only name for example, "configname" for file "configname.conf"
configFile=$1

if [ -z "$1" ]; then echo "missing config file name"; exit 1; fi

# config file need to contains this variables
# sourceDomainCatalog=""
# sqlUsername=""
# sqlPassword=""
# sqlDatabaseName=""

# import config
. settings/domains/$configFile.conf

domainName=$1

# check for must have variables
if [ -z "$sourceDomainCatalog" ]; then echo "missing sourceDomainCatalog"; exit 1; fi
if [ -z "$sqlUsername" ]; then echo "missing sqlUsername"; exit 1; fi
if [ -z "$sqlUsername" ]; then echo "missing sqlUsername"; exit 1; fi
if [ -z "$sqlDatabaseName" ]; then echo "missing sqlDatabaseName"; exit 1; fi

#set backup catalog
backupCatalog="domains/$domainName"

#create directory
mkdir -p $backupCatalog

echo -n "  backup DIR => START "

# Backup www directory
tar -cpzf $backupCatalog/www-`date +%Y-%m-%d`.tar.gz -C ~/$sourceDomainCatalog .

echo -e "\r  backup DIR => DONE "

echo -n "  backup DB => START "

# Backup database
# mysqldump --databases -u $sqlUsername --password=$sqlPassword $sqlDatabaseName > $backupCatalog/db-`date +%Y-%m-%d`.sql
mysqldump --host mysql39.mydevil.net --password=$sqlPassword --user $sqlUsername $sqlDatabaseName > $backupCatalog/db-`date +%Y-%m-%d`.sql

echo -e "\r  backup DB => DONE "

echo -n "  cleanup => START "

# Delete old backups
find $backupCatalog/ -mtime +30 -delete

echo -e "\r  cleanup DB => DONE "
