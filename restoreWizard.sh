#!/bin/bash

function strip {
    local STRING=${1#$"$2"}
    echo ${STRING%$"$2"}
}

printLogo () {

    echo ''
    echo '        ____  ___   ________ ____  ______  __________ '
    echo '       / __ )/   | / ____/ //_/ / / / __ \/ ____/ __ \'
    echo '      / __  / /| |/ /   / ,< / / / / /_/ / __/ / /_/ /'
    echo '     / /_/ / ___ / /___/ /| / /_/ / ____/ /___/ _, _/ '
    echo '    /_____/_/  |_\____/_/ |_\____/_/   /_____/_/ |_|  '
    echo ''

}

printSpacer () {
    echo ""
    echo "=================="
    echo ""
}

domainPicker () {
    printSpacer
    title="Select domain"
    prompt="Pick domain:"

    options=()
    for dirname in ./domains/* ; do
        domain=$(strip "$dirname" "./domains/")
        # echo "$domain"
        options+=($domain)
    done

    echo "$title"
    PS3="$prompt "
    select opt in "${options[@]}" "Quit"; do 

        case "$REPLY" in

        $(( ${#options[@]}+1 )) ) echo "Goodbye!"; break;;
        *) 
            # echo "You picked $opt which is option $REPLY"
            backupDatePicker $opt
        break;;

        esac

    done
    
}

backupDatePicker () {
    printSpacer
    # echo "domain: $1, select date to backup:"

    title="Select date to backup:"
    prompt="Pick date:"

    options=()
    for dirname in ./domains/$1/www-* ; do
        filename=$(strip "$dirname" "./domains/$1/www-")
        date=$(strip "$filename" ".tar.gz")
        # echo "$domain"
        options+=($date)
    done

    echo "$title"
    PS3="$prompt "
    select opt in "${options[@]}" "Quit"; do 

        case "$REPLY" in

        $(( ${#options[@]}+1 )) ) echo "Goodbye!"; break;;
        *) 
            # echo "You picked $opt which is option $REPLY"
            restoreBackup $opt $1
        break;;

        esac

    done

}

restoreBackup () {
    printSpacer
    echo "restore from $1 file from domain $2" 

    . settings/domains/$2.config
    
    domainName=$2

    read -p "backup files? [y/Y] [n/N]" -n 1 -r
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        tar -xpzf ./domains/$domainName/www-$1.tar.gz -C ~/$sourceDomainCatalog
    fi

    read -p "backup database? [y/Y] [n/N]" -n 1 -r
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        mysql -u $sqlUsername --password=$sqlPassword $sqlDatabaseName < ./domains/$domainName/db-$1.sql
    fi

}

printLogo
domainPicker
