#!/bin/bash


configFile=$1
date=$2
plcoudCode=llQ7ZJqOxzlA7jFzHXFHY0uDbbyGmpcS7

if [ -z "$1" ]; then echo "missing config file name"; exit 1; fi
if [ -z "$2" ]; then date=`date +%Y-%m-%d`; fi

# config file need to contains this variables
# sourceDomainCatalog=""

# import config
. settings/domains/$configFile.conf

domainName=$1

# check for must have variables
if [ -z "$sourceDomainCatalog" ]; then echo "missing sourceDomainCatalog"; exit 1; fi

#set backup catalog
backupCatalog="domains/$domainName"

echo -n "  Upload => START "

curl -X POST \
  "https://api.pcloud.com/uploadtolink?code=$plcoudCode&names=$domainName" \
  -H 'cache-control: no-cache' \
  -H 'content-type: multipart/form-data;' \
  -F "files[]=@$backupCatalog/db-$date.sql" \
  -F "files[]=@$backupCatalog/www-$date.tar.gz"
  
echo -e "\r  Upload => DONE "