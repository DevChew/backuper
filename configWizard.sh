#!/bin/bash

function getByHand {

    echo "domain name ex: example.com"
    echo ""

    read domainName

    echo "================="
    echo "sourceDomainCatalog ex: domains/example.com/htdocs"
    echo "witchout slash on start or end"
    echo ""
    read sourceDomainCatalog

    echo "================="
    echo "sqlUsername ex: root"
    echo ""
    read sqlUsername

    echo "================="
    echo "sqlDatabaseName ex: database"
    echo ""
    read sqlDatabaseName

    echo "================="
    echo "sqlPassword"
    echo ""
    read -s sqlPassword

    store
}

function getFromWpConfig {

    echo "input path to wp-config.php witchout wp-config.php"
    echo "================="

    read wpConfig

    if [ -f "$wpConfig/wp-config.php" ]
    then

        sqlDatabaseName=`cat $wpConfig/wp-config.php | grep DB_NAME | cut -d \' -f 4`
        sqlUsername=`cat $wpConfig/wp-config.php | grep DB_USER | cut -d \' -f 4`
        sqlPassword=`cat $wpConfig/wp-config.php | grep DB_PASSWORD | cut -d \' -f 4`

        echo "domain name ex: example.com"
        echo ""

        read domainName

        echo "================="
        echo "sourceDomainCatalog ex: domains/example.com/htdocs"
        echo "witchout slash on start or end"
        echo ""
        read sourceDomainCatalog

        store
    else
        echo ""
        echo "file not exist, ty again"
        echo ""
        getFromWpConfig
    fi


}

function store {

echo "================="
echo "creating file $domainName.config"

cat > ./settings/domains/$domainName.conf << EOF
sourceDomainCatalog=$sourceDomainCatalog
sqlUsername=$sqlUsername
sqlPassword=$sqlPassword
sqlDatabaseName=$sqlDatabaseName
EOF

echo "DONE"

}

case "$1" in
  "wp")getFromWpConfig ;;
  *) getByHand ;;
esac
