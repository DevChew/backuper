#!/bin/bash

function strip {
    local STRING=${1#$"$2"}
    echo ${STRING%$"$2"}
}

# create domain catalog
mkdir -p domains

# loop throug all domains in settings
configs=()
for filepath in ./settings/domains/*.conf ; do
    config=$(strip "$filepath" "./settings/domains/")
    configname=$(strip "$config" ".conf")
    # echo "$domain"
    configs+=($configname)
done

echo -e "\e[33mBackup all domains \e[39m"

# run singleBackup.sh [configname] for all configs
for cfg in "${configs[@]}"
do
	echo "$cfg"
	sh singleBackup.sh $cfg
done


echo -e "\e[33mUpload all domains \e[39m"

# run singleUpload.sh [configname] for all configs
for cfg in "${configs[@]}"
do
	echo "$cfg "
	sh singleUpload.sh $cfg
done
